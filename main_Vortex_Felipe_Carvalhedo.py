#Os inputs necessários
time_left = int(input("\nDigite o número de segundos da jogada, o valor deve ser um número inteiro entre 1 e 24: \n"))
number_players = int(input("\nDigite o número de jogadores em quadra, o valor deve ser um número inteiro entre 2 e 5: \n"))
repositioning_time = int(input(f"\nDigite o tempo de reposicionamento dos jogadores após o passe, o valor deve ser um número inteiro maior do que zero e menor do que {time_left}: \n"))

#Definição de variáveis para o somatório
avaible_players = number_players
score_count = 0
current_score = 0

while time_left > 0:
    avaible_players -=1
    current_score = avaible_players
    if avaible_players == (number_players - repositioning_time):
        current_score = avaible_players
        avaible_players += 1 
    score_count += current_score
    time_left -= 1

print(f"\nO somatório dos jogadores livres para cada passe no treino do Mafrashow é : {score_count}\n")